const express = require('express');

const voucherMiddleware=require('../middlewares/voucherMiddleware')

const router=express.Router();

router.use((req,res,next)=>{
    console.log("Request URL review: "+ req.url);
    next();
})

router.get("/",voucherMiddleware.getAllVoucherMiddleware, (req,res) =>{
    res.json({
        message:"Get all voucher"
    })
})

router.post("/",voucherMiddleware.createVoucherMiddleware,(req,res) =>{
    res.json({
        message:"Create voucher"
    })
})

router.get("/:voucherId",voucherMiddleware.getDetailVoucherMiddleware,(req,res) =>{
    var voucherId=req.params.voucherId;

    res.json({
        message:"Get voucher id = " + voucherId
    })
})


router.put("/:voucherId",voucherMiddleware.updateVoucherMiddleware,(req,res) =>{
    var voucherId=req.params.voucherId;

    res.json({
        message:"Update voucher id = " + voucherId
    })
})

router.delete("/:voucherId",voucherMiddleware.deleteVoucherMiddleware,(req,res) =>{
    var voucherId=req.params.voucherId;

    res.json({
        message:"Get voucher id = " + voucherId
    })
})
//export router
module.exports=router;