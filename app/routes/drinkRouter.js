const express = require('express');

const drinkMiddleware=require("../middlewares/drinkMiddleware");

const router=express.Router();

router.use((req,res,next)=>{
    console.log("Request URL: "+ req.url);
    next();
})

router.get("/",drinkMiddleware.getAllDrinkMiddleware, (req,res) =>{
    res.json({
        message:"Get all drinks"
    })
})

router.post("/",drinkMiddleware.createDrinkMiddleware,(req,res) =>{
    res.json({
        message:"Create drinks"
    })
})

router.get("/:drinkId",drinkMiddleware.getDetailDrinkMiddleware,(req,res) =>{
    var drinkId=req.params.drinkId;

    res.json({
        message:"Get drink id = " + drinkId
    })
})


router.put("/:drinkId",drinkMiddleware.updateDrinkMiddleware,(req,res) =>{
    var drinkId=req.params.drinkId;

    res.json({
        message:"Update drink id = " + drinkId
    })
})

router.delete("/:drinkId",drinkMiddleware.deleteDrinkMiddleware,(req,res) =>{
    var drinkId=req.params.drinkId;

    res.json({
        message:"Delete drink id = " + drinkId
    })
})
//export router
module.exports=router;