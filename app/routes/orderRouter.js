const express = require('express');

const orderMiddleware=require('../middlewares/orderMiddleware')

const router=express.Router();

router.use((req,res,next)=>{
    console.log("Request URL review: "+ req.url);
    next();
})

router.get("/",orderMiddleware.getAllOrderMiddleware, (req,res) =>{
    res.json({
        message:"Get all order"
    })
})

router.post("/",orderMiddleware.createOrderMiddleware,(req,res) =>{
    res.json({
        message:"Create order"
    })
})

router.get("/:orderId",orderMiddleware.getDetailOrderMiddleware,(req,res) =>{
    var orderId=req.params.userId;

    res.json({
        message:"Get order id = " + orderId
    })
})


router.put("/:orderId",orderMiddleware.updateOrderMiddleware,(req,res) =>{
    var orderId=req.params.orderId;

    res.json({
        message:"Update order id = " + orderId
    })
})

router.delete("/:orderId",orderMiddleware.deleteOrderMiddleware,(req,res) =>{
    var orderId=req.params.orderId;

    res.json({
        message:"Get order id = " + orderId
    })
})
//export router
module.exports=router;