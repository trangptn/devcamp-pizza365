const express = require('express');

const userMiddleware=require('../middlewares/userMiddleware')

const router=express.Router();

router.use((req,res,next)=>{
    console.log("Request URL review: "+ req.url);
    next();
})

router.get("/",userMiddleware.getAllUserMiddleware, (req,res) =>{
    res.json({
        message:"Get all user"
    })
})

router.post("/",userMiddleware.createUserMiddleware,(req,res) =>{
    res.json({
        message:"Create user"
    })
})

router.get("/:userId",userMiddleware.getDetailUserMiddleware,(req,res) =>{
    var userId=req.params.userId;

    res.json({
        message:"Get user id = " + userId
    })
})


router.put("/:userId",userMiddleware.updateUserMiddleware,(req,res) =>{
    var userId=req.params.userId;

    res.json({
        message:"Update user id = " + userId
    })
})

router.delete("/:userId",userMiddleware.deleteUserMiddleware,(req,res) =>{
    var userId=req.params.userId;

    res.json({
        message:"Get user id = " + userId
    })
})
//export router
module.exports=router;