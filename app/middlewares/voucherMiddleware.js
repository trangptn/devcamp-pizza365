const getAllVoucherMiddleware=(req,res,next)=>{
    console.log("Get all voucher Middleware");

    next();
}

const createVoucherMiddleware=(req,res,next)=>{
    console.log("Create voucher Middleware");

    next();
}

const getDetailVoucherMiddleware=(req,res,next)=>{
    console.log("Get detail voucher Middleware");

    next();
}

const updateVoucherMiddleware=(req,res,next)=>{
    console.log("Update voucher Middleware");

    next();
}

const deleteVoucherMiddleware=(req,res,next)=>{
    console.log("Delete voucher Middleware");

    next();
}

module.exports ={
    getAllVoucherMiddleware,
    getDetailVoucherMiddleware,
    createVoucherMiddleware,
    updateVoucherMiddleware,
    deleteVoucherMiddleware
}