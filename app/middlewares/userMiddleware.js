const getAllUserMiddleware=(req,res,next)=>{
    console.log("Get all user Middleware");

    next();
}

const createUserMiddleware=(req,res,next)=>{
    console.log("Create user Middleware");

    next();
}

const getDetailUserMiddleware=(req,res,next)=>{
    console.log("Get detail user Middleware");

    next();
}

const updateUserMiddleware=(req,res,next)=>{
    console.log("Update user Middleware");

    next();
}

const deleteUserMiddleware=(req,res,next)=>{
    console.log("Delete user Middleware");

    next();
}

module.exports ={
    getAllUserMiddleware,
    getDetailUserMiddleware,
    createUserMiddleware,
    updateUserMiddleware,
    deleteUserMiddleware
}