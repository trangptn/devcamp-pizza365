const getAllDrinkMiddleware=(req,res,next)=>{
    console.log("Get all drink Middleware");

    next();
}

const createDrinkMiddleware=(req,res,next)=>{
    console.log("Create drink Middleware");

    next();
}

const getDetailDrinkMiddleware=(req,res,next)=>{
    console.log("Get detail drink Middleware");

    next();
}

const updateDrinkMiddleware=(req,res,next)=>{
    console.log("Update drink Middleware");

    next();
}

const deleteDrinkMiddleware=(req,res,next)=>{
    console.log("Delete drink Middleware");

    next();
}

module.exports= {
    getAllDrinkMiddleware,
    getDetailDrinkMiddleware,
    createDrinkMiddleware,
    updateDrinkMiddleware,
    deleteDrinkMiddleware
}