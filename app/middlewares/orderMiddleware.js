const getAllOrderMiddleware=(req,res,next)=>{
    console.log("Get all order Middleware");

    next();
}

const createOrderMiddleware=(req,res,next)=>{
    console.log("Create order Middleware");

    next();
}

const getDetailOrderMiddleware=(req,res,next)=>{
    console.log("Get detail order Middleware");

    next();
}

const updateOrderMiddleware=(req,res,next)=>{
    console.log("Update order Middleware");

    next();
}

const deleteOrderMiddleware=(req,res,next)=>{
    console.log("Delete order Middleware");

    next();
}

module.exports ={
    getAllOrderMiddleware,
    getDetailOrderMiddleware,
    createOrderMiddleware,
    updateOrderMiddleware,
    deleteOrderMiddleware
}